package com.cxs;

import com.alibaba.fastjson.JSONObject;
import com.cxs.common.result.ResultVO;
import com.cxs.service.IUserService;
import com.cxs.utils.JsonUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;

@EnableCaching
@SpringBootTest
class SpringbootSecurityApplicationTests {

    @Autowired
    IUserService userService;


    @Test
    void test() {
        String jsonStr = JsonUtil.readJsonFile("/json/formAttr.json");
        JSONObject result = JSONObject.parseObject(jsonStr);
    }

}
