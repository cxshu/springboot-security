package com.cxs.security;
import com.cxs.utils.RedisUtil;
import com.cxs.common.lang.Const;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.cxs.common.exception.CaptchaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 图片验证码校验过滤器，在登录过滤器前
 * 所有请求都会通过 OncePerRequestFilter 中的 doFilterInternal方法，且只通过一次
 */
@Component
public class CaptchaFilter extends OncePerRequestFilter {
    private final String loginUrl = "/login";
    @Autowired
    RedisUtil redisUtil;
    @Autowired
    LoginFailureHandler loginFailureHandler;

    /**
     * 拦截器，拦截所有请求
     * 当请求为"/login"时，检验验证码，如果验证码不正确，则调用认证失败处理器，然后放行；如果正确，则直接放行
     * 当为其他请求时，放行
     * @param httpServletRequest
     * @param httpServletResponse
     * @param filterChain
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain)
            throws ServletException, IOException {
        String url = httpServletRequest.getRequestURI();
        if ("/login".equals(url) && httpServletRequest.getMethod().equals("POST")) {
            // 校验验证码
            try {
                validate(httpServletRequest);
            } catch (CaptchaException e) {
                // 校验验证码时抛出的异常在这里被捕获到，然后发送给登录失败处理器
                loginFailureHandler.onAuthenticationFailure(httpServletRequest, httpServletResponse, e);
            }
        }
        // 放行
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    /**
     * 校验验证码逻辑
     * @param httpServletRequest
     */
    private void validate(HttpServletRequest httpServletRequest) {
        // 用户登录时传过来的验证码
        String code = httpServletRequest.getParameter("code");
        // 用户登录时传过来的验证码的token，也就是后端redis中存储验证码的key值
        String key = httpServletRequest.getParameter("code_token");
        // 判断验证码和key是否为空
        if (StringUtils.isBlank(code) || StringUtils.isBlank(key)) {
            throw new CaptchaException();
        }
        // 通过key从redis中取出验证码，和客户端传过来的验证码进行比较
        if (!code.equals(redisUtil.hget(Const.CAPTCHA_KEY, key))) {
            // 如果验证码不相等，则抛出我们自定义的验证码异常
            throw new CaptchaException();
        }

        // 如果验证码正确，则删除
        redisUtil.hdel(Const.CAPTCHA_KEY, key);
    }
}

