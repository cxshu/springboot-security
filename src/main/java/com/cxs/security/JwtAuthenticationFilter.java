package com.cxs.security;

import cn.hutool.core.util.StrUtil;
import com.cxs.entity.User;
import com.cxs.service.IUserService;
import com.cxs.utils.JwtUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class JwtAuthenticationFilter extends BasicAuthenticationFilter {

    @Autowired
    JwtUtil jwtUtil;

    @Autowired
    UserDetailServiceImpl userDetailService;

    @Autowired
    IUserService userService;


    public JwtAuthenticationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        System.out.println("JwtAuthenticationFilter " + request.getRequestURI());
        // 从配置文件中获取header的值，然后通过header获取jwt
        String jwt = request.getHeader(jwtUtil.getHeader());
        if (StrUtil.isBlankOrUndefined(jwt)) {
            // 如果jwt为空，则放行
            chain.doFilter(request, response);
            return;
        }

        Claims claims = jwtUtil.getClaimByToken(jwt);
        if (claims == null) {
            throw new JwtException("token 异常");
        }
        if (jwtUtil.isTokenExpired(claims)) {
            throw new JwtException("token 已过期！");
        }

        String username = claims.getSubject();
        // 获取用户的权限等信息

        User user = userService.getByUsername(username);

        // 生成token
        UsernamePasswordAuthenticationToken token
                = new UsernamePasswordAuthenticationToken(username, null, userDetailService.getUserAuthority(user.getId()));

        // 设置认证主体
        SecurityContextHolder.getContext().setAuthentication(token);
        // 过滤器放行
        chain.doFilter(request, response);
    }
}
