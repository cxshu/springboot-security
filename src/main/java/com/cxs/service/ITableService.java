package com.cxs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cxs.entity.Table;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chaoxiaoshu
 * @since 2022-07-05
 */
public interface ITableService extends IService<Table> {

}
