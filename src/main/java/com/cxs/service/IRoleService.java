package com.cxs.service;

import com.cxs.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cxs.mapper.RoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chaoxiaoshu
 * @since 2022-06-08
 */
public interface IRoleService extends IService<Role> {

    /**
     * 通过用户获取角色列表<br>
     * 优先缓存
     * @param userId
     * @return
     */
    List<Role> findRolesByUserId(Long userId);

    List<Integer> findPermissionIdsByRoleId(Long roleId);

    Role createRole(Role r);
}
