package com.cxs.service.impl;

import com.cxs.common.dto.NodeDto;
import com.cxs.common.lang.Const;
import com.cxs.entity.Role;
import com.cxs.entity.User;
import com.cxs.mapper.PermissionMapper;
import com.cxs.mapper.RoleMapper;
import com.cxs.mapper.UserMapper;
import com.cxs.service.IRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cxs.service.IUserService;
import com.cxs.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chaoxiaoshu
 * @since 2022-06-08
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {
    @Autowired
    RoleMapper roleMapper;

    @Autowired
    UserMapper userMapper;

    @Autowired
    PermissionMapper permissionMapper;

    @Autowired
    IUserService userService;

    @Autowired
    RedisUtil redisUtil;

    @Override
    public List<Role> findRolesByUserId(Long userId) {
        return roleMapper.findRolesByUserId(userId);
    }

    @Override
    public List<Integer> findPermissionIdsByRoleId(Long roleId) {
        List<Integer> menuIds = permissionMapper.findPermissionIdsByRoleId(roleId);
        return menuIds;
    }


    @Override
    public Role createRole(Role r) {
        Role role = new Role();
        if (r.getId() != null) {
            role.setId(r.getId());
        }
        if (r.getName() != null) {
            role.setName(r.getName());
        }
        if (r.getCode() != null) {
            role.setCode(r.getCode());
        }
        if (r.getRemark() != null) {
            role.setRemark(r.getRemark());
        }
        if (r.getStatus() != null) {
            role.setStatus(r.getStatus());
        }
        return role;
    }
}
