package com.cxs.service.impl;

import com.cxs.entity.Table;
import com.cxs.mapper.TableMapper;
import com.cxs.service.ITableService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chaoxiaoshu
 * @since 2022-07-05
 */
@Service
public class TableServiceImpl extends ServiceImpl<TableMapper, Table> implements ITableService {

}
