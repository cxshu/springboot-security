package com.cxs.service.impl;

import com.cxs.entity.RolePerm;
import com.cxs.mapper.RolePermMapper;
import com.cxs.service.IRolePermService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chaoxiaoshu
 * @since 2022-06-14
 */
@Service
public class RolePermServiceImpl extends ServiceImpl<RolePermMapper, RolePerm> implements IRolePermService {

}
