package com.cxs.service.impl;

import com.cxs.entity.UserRole;
import com.cxs.mapper.UserRoleMapper;
import com.cxs.service.IUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chaoxiaoshu
 * @since 2022-06-14
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements IUserRoleService {

}
