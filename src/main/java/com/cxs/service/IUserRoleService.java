package com.cxs.service;

import com.cxs.entity.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chaoxiaoshu
 * @since 2022-06-14
 */
public interface IUserRoleService extends IService<UserRole> {

}
