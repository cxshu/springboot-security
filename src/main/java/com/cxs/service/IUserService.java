package com.cxs.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cxs.entity.Permission;
import com.cxs.entity.Role;
import com.cxs.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chaoxiaoshu
 * @since 2022-06-08
 */
public interface IUserService extends IService<User> {

    // 为了防止不同方法的key重复，最好给key加前缀
    @Cacheable(value = "cache_user", key = "'userId-'+#userId")
    User getById(Long userId);

    @Cacheable(value = "cache_user", key = "'username-'+#username")
    User getByUsername(String username);

    @Caching(evict = {@CacheEvict(value = "cache_user", key = "'userId-'+#user.id"),
            @CacheEvict(value = "cache_user", key = "'username-'+#user.username")})
    Integer updateUserById(User user);

}
