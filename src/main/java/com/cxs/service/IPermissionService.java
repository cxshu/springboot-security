package com.cxs.service;

import com.cxs.common.dto.MenuDto;
import com.cxs.common.dto.NodeDto;
import com.cxs.entity.Permission;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cxs.entity.Role;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chaoxiaoshu
 * @since 2022-06-08
 */
public interface IPermissionService extends IService<Permission> {

    /**
     * 获取所有目录和菜单
     * @return
     */
    List<Permission> findAllMenu(int[] types);




    /**
     * 获取指定用户的所有菜单<br>
     * @param userId
     * @return
     */
    List<Permission> findMenusByUserId(Long userId);
    /**
     * 获取指定用户的所有权限<br>
     * @param userId
     * @return
     */
    List<Permission> findPermissionsByUserId(Long userId);


    /**
     * 获取用户权限信息（角色编码、菜单权限组成的字符串）
     * 如： ROLE_admin,sys:manager,sys:user:list  角色: admin,菜单权限: sys:manager,sys:user:list
     * @param userId
     * @return
     */
    String getAuthorityByUserId(Long userId);

    /**
     * 获取指定用户的菜单
     * @param userId
     * @return
     */
    List<MenuDto> getMenu(Long userId);

    /**
     * 获取树形组件的数据
     * @return
     */
    List<NodeDto> getNodes();

    /**
     * 把数据转成树形结构
     * @param permissions
     * @return
     */
    List<Permission> buildTree(List<Permission> permissions);

    void clearCacheByPermissionId(Long permissionId);

    void clearCacheByRoleId(Long roleId);
}
