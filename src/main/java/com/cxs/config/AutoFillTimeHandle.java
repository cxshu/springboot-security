package com.cxs.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import java.time.LocalDateTime;

@Configuration
public class AutoFillTimeHandle implements MetaObjectHandler {

    // 插入时间
    @Override
    public void insertFill(MetaObject metaObject) {
        // 第一个参数对应的是实体类里的成员变量的名称
        // 如果
        this.setFieldValByName("createTime", LocalDateTime.now(), metaObject);
        this.setFieldValByName("updateTime", LocalDateTime.now(), metaObject);
    }

    // 更新时间
    @Override
    public void updateFill(MetaObject metaObject) {
        this.setFieldValByName("updateTime", LocalDateTime.now(), metaObject);
        this.setFieldValByName("lastLogin", LocalDateTime.now(), metaObject);
    }
}
