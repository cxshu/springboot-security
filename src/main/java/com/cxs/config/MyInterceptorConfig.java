package com.cxs.config;

import com.cxs.config.interceptor.LastloginInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

//@Configuration
public class MyInterceptorConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(getLastloginInterceptor()).addPathPatterns("/**");
    }

//    @Bean
    public LastloginInterceptor getLastloginInterceptor() {
        return new LastloginInterceptor();
    }
}
