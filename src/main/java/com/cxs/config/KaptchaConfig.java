package com.cxs.config;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

/**
 * 配置Kaptcha
 */
@Configuration
public class KaptchaConfig {

    /**
     * 验证码图片的配置
     * @return
     */
    @Bean
    public DefaultKaptcha defaultKaptcha() {
        Properties properties = new Properties();
        // 是否有边框
        properties.put("kaptcha.border", "no");
        // 文本颜色
        properties.put("kaptcha.textproducer.font.color", "black");
        // 干扰颜色
        properties.put("kaptcha.noise.color", "green");
        // 每个文本的间距
        properties.put("kaptcha.textproducer.char.space", "4");
        // 高度
        properties.put("kaptcha.image.height", "40");
        // 宽度
        properties.put("kaptcha.image.width", "120");
        // 字体大小
        properties.put("kaptcha.textproducer.font.size", "30");

        Config config = new Config(properties);
        DefaultKaptcha defaultKaptcha = new DefaultKaptcha();
        defaultKaptcha.setConfig(config);
        return defaultKaptcha;
    }
}
