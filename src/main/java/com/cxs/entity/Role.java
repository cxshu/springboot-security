package com.cxs.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author chaoxiaoshu
 * @since 2022-06-08
 */
@Data
@TableName("sys_role")
public class Role extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 角色名称
     */
    private String name;

    private String code;

    private String remark;

    /**
     * 该角色关联的权限的id列表
     */
//    @TableField(exist = false)
//    private List<Integer> menuIds = null;

}
