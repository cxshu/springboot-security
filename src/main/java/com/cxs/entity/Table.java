package com.cxs.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cxs.entity.BaseEntity;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author chaoxiaoshu
 * @since 2022-07-05
 */
@Getter
@Setter
@TableName("sys_table")
public class Table {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 菜单id，表明此列属于哪一个菜单
     */
    private Long permissionId;

    /**
     * 父列id，值为0时为非嵌套列。默认为0
     */
    private Long parentId;

    /**
     * 列字段名称
     */
    private String field;

    /**
     * 列标题
     */
    private String label;

    /**
     * 列类型，avatar, input, tag, tags, handle
     */
    private String type;

    /**
     * 列宽
     */
    private Integer width;

    /**
     * 列头组件类型 input,select,daterange
     */
    private String header;

    /**
     * 是否排序 true,false
     */
    private Integer sortable;

    /**
     * 对齐方式 left, center, right
     */
    private String fixed;

    /**
     * 操作列
     */
    private String handle;


}
