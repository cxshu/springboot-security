package com.cxs.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author chaoxiaoshu
 * @since 2022-06-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_permission")
public class Permission extends BaseEntity {

    private static final long serialVersionUID = 1L;

    private Long parentId;

    /**
     * 数据库表名
     */
    private String tableName;

    /**
     * 菜单或按钮的名称，如：用户管理、角色管理
     */
    private String name;

    /**
     * url，如/user
     */
    private String path;

    /**
     * 授权（多个用逗号分隔，如user:list,user:create)
     */
    private String perms;

    /**
     * 组件路径
     */
    private String component;

    /**
     * 类型， 0：目录； 1：菜单； 2：按钮
     */
    private Integer type;

    /**
     * 菜单图标
     */
    private String icon;

    /**
     * 排序
     */
    private Integer orderNum;

    @TableField(exist = false)
    private List<Permission> children = new ArrayList<>();

    public void addChildren(Permission p) {
        this.children.add(p);
    }

}
