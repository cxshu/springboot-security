package com.cxs.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cxs.entity.BaseEntity;
import java.io.Serializable;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author chaoxiaoshu
 * @since 2022-06-14
 */
@Getter
@Setter
@TableName("sys_role_perm")
public class RolePerm {

    @TableId(value = "id", type = IdType.AUTO)
    protected Long id;

    /**
     * 角色表id
     */
    private Long roleId;

    /**
     * 权限表id
     */
    private Long permId;

    public RolePerm(Long roleId, Long permId) {
        this.roleId = roleId;
        this.permId = permId;
    }
}
