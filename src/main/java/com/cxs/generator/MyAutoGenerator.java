package com.cxs.generator;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.baomidou.mybatisplus.generator.fill.Column;
import com.baomidou.mybatisplus.generator.fill.Property;
import com.cxs.controller.base.BaseController;
import com.cxs.entity.BaseEntity;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.util.Collections;

public class MyAutoGenerator {
    /*
        数据源配置
     */
    final static String url = "jdbc:mysql://127.0.0.1:3306/" +
            "springsecurity?useUnicode=true&characterEncoding=utf8&" +
            "autoReconnect=true&allowMultiQueries=true&useSSL=false";       // 数据源url
    final static String username = "root";  // 数据源username
    final static String password = "root123";  // 数据源密码

    /*
        全局配置
     */
    final static String author = "chaoxiaoshu";  // 注释作者

    /*
        包配置
     */
    final static String parentPackage = "com.cxs";  // 设置父包名
    final static String xmlPath = "/mapper";  // 设置xml映射器的路径

    final static String[] tables = {
            "sys_table", "sys_table_detail"
    };



    public static void main(String[] args) throws IOException {
        String mainDir = getPath();
        for (String table : tables) {
            generateUser(table, "sys_");
        }
    }

    /**
     * 代码生成器
     * @param tableName
     * @param tablePrefix
     */
    private static void generateUser(String tableName, String tablePrefix) {
        String mainDir = getPath();
        String outputDir = mainDir + "/java/";
        String xmlDir = mainDir + "/resources" + xmlPath;

        FastAutoGenerator.create(url, username, password)
                // 全局配置
                .globalConfig(builder -> {
                    builder.author(author)  // 设置作者
//                            .enableSwagger() // 开启 swagger 模式
                            .outputDir(outputDir)  // 设置输出目录
                            .disableOpenDir()  // 禁止打开输出目录
                    ;
                })
                // 包配置
                .packageConfig(builder -> {
                    builder.parent(parentPackage) // 设置父包名
                            // 设置父包模块名
//                          .moduleName("system")
                            // 设置Xml映射器生成路径
                            .pathInfo(Collections.singletonMap(OutputFile.xml, xmlDir));
                })
                // 策略配置
                .strategyConfig(builder -> {
                    builder.addInclude(tableName)  // 设置需要生成的表名
                            // 设置过滤表前缀，生成的实体类的名称就是tableName去掉前缀
                            // 例如：tb_user去掉前缀tb_后生成的实体类名为User
                            .addTablePrefix(tablePrefix);
                })
                // 实体类策略配置
                .strategyConfig(builder -> {
                    builder.entityBuilder()
                            .enableLombok()   // 开启lombok
                            .addSuperEntityColumns("id", "create_time", "update_time", "version", "deleted")
                            .superClass(BaseEntity.class);  // 生成的实体类继承父类BaseEntity
//                            .versionColumnName("version")  // 乐观锁字段名(数据库)
//                            .versionPropertyName("version")  // 乐观锁属性名(实体)
//                            .logicDeleteColumnName("deleted")  // 逻辑删除字段名(数据库)
//                            .logicDeletePropertyName("deleted")  // 逻辑删除属性名(实体)
//                            .addTableFills(new Column("create_time", FieldFill.INSERT))  // 添加表字段填充
//                            .addTableFills(new Column("update_time", FieldFill.INSERT_UPDATE));  // 添加表字段填充
                })
                // Mapper策略配置
                .strategyConfig(builder -> {
                    builder.mapperBuilder()
                            .enableMapperAnnotation();  // Mapper层开启 @Mapper 注解
                })
                // Controller策略配置
                .strategyConfig(builder -> {
                    builder.controllerBuilder()
                            .superClass(BaseController.class)
                            .enableRestStyle();   // 开启生成@RestController 控制器
                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();
    }

    /**
     * 获取项目或模块的路径
     * @return
     */
    private static String getPath(){
        Resource resource = new ClassPathResource("");
        try {
            String absolutePath = resource.getFile().getAbsolutePath();
            String str = absolutePath.replace("\\target\\classes", "\\src\\main");
            return str;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}