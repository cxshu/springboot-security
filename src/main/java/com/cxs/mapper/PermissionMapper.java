package com.cxs.mapper;

import com.cxs.common.dto.NodeDto;
import com.cxs.entity.Permission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cxs.entity.Role;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chaoxiaoshu
 * @since 2022-06-08
 */
@Mapper
public interface PermissionMapper extends BaseMapper<Permission> {

    List<Permission> findPermissionsByUserId(Long userId);

    List<Permission> findMenusByUserId(Long userId);

    List<Integer> findPermissionIdsByRoleId(Long roleId);

}
