package com.cxs.mapper;

import com.cxs.entity.RolePerm;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chaoxiaoshu
 * @since 2022-06-14
 */
@Mapper
public interface RolePermMapper extends BaseMapper<RolePerm> {
    List<RolePerm> findAllByRoleId(Long roleId);
}
