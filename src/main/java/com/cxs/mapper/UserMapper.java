package com.cxs.mapper;

import com.cxs.common.dto.TableDto;
import com.cxs.entity.Permission;
import com.cxs.entity.Role;
import com.cxs.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cxs.entity.UserRole;
import org.apache.ibatis.annotations.Mapper;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chaoxiaoshu
 * @since 2022-06-08
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

    List<User> findUsersByRoleId(Long roleId);

    List<User> findUsersByPermissionId(Long permissionId);

    List<TableDto> getUserTableColumn();

}
