package com.cxs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cxs.common.dto.FormDto;
import com.cxs.common.dto.TableDto;
import com.cxs.entity.Table;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chaoxiaoshu
 * @since 2022-07-05
 */
@Mapper
public interface TableMapper extends BaseMapper<Table> {
    List<FormDto.Option> getTableColumn(String tableName);
}
