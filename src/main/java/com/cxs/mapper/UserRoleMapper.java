package com.cxs.mapper;

import com.cxs.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chaoxiaoshu
 * @since 2022-06-14
 */
@Mapper
public interface UserRoleMapper extends BaseMapper<UserRole> {
    List<UserRole> findAllByUserId(Long userId);
}
