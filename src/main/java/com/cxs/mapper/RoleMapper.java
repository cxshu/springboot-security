package com.cxs.mapper;

import com.cxs.common.dto.NodeDto;
import com.cxs.entity.Permission;
import com.cxs.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chaoxiaoshu
 * @since 2022-06-08
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {
   List<Role> findRolesByUserId(Long userId);
}
