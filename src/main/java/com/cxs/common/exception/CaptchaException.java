package com.cxs.common.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * 验证码异常
 */
public class CaptchaException extends AuthenticationException {
    private String code;

    public CaptchaException() {
        this("");
    }

    public CaptchaException(String code) {
        super("captcha exception");
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
