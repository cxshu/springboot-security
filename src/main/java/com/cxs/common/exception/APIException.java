package com.cxs.common.exception;

import lombok.Getter;

/**
 * 自定义异常
 */
@Getter
public class APIException extends RuntimeException {
    private String code = "A0001";
    private String param = "";

    public APIException() {
    }

    public APIException(String code) {
        this.code = code;
    }

    public APIException(String code, String param) {
        this.code = code;
        this.param = param;
    }

}