package com.cxs.common.exception;

import lombok.Getter;
import org.springframework.security.core.AuthenticationException;

@Getter
public class MyLoginException extends AuthenticationException {
    private String code;

    public MyLoginException(String code) {
        super("");
        this.code = code;
    }
}
