package com.cxs.common.result;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * 错误码相关方法
 */
public class ErrorCodeUtils {
    private final static String basePath = "i18n/messages";

    private ErrorCodeUtils(){}

    /**
     * 获得错误码对应的提示信息
     * @param errCode 错误码
     * @param params 匹配参数
     * @return 返回错误提示信息
     */
    public static String getMessage(String errCode, String ...params) {
        ResourceBundle bundle = ResourceBundle.getBundle(basePath, Locale.getDefault());
        String msg = bundle.getString(errCode);
        MessageFormat messageFormat = new MessageFormat(msg);
        return messageFormat.format(params);
    }
}
