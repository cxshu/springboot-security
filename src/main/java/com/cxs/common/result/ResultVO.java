package com.cxs.common.result;

import lombok.Data;

/**
 * 统一返回的数据封装类
 * @param <T>
 */
@Data
public class ResultVO<T> {
    /**
     * 状态码
     */
    private String code = "00000";
    /**
     * 响应信息
     */
    private String msg = "";
    /**
     * 响应的具体数据
     */
    private T data = null;

    /**
     * 一切OK
     * @param data
     */
    public ResultVO(T data) {
        this.msg = ErrorCodeUtils.getMessage(this.code);
        this.data = data;
    }

    public ResultVO(String code) {
        this.code = code;
        this.msg = ErrorCodeUtils.getMessage(this.code);
    }

    public ResultVO(String code, T data) {
        this.code = code;
        this.msg = ErrorCodeUtils.getMessage(this.code);
        this.data = data;
    }

    public ResultVO(String code, String param, T data) {
        this.code = code;
        this.msg = ErrorCodeUtils.getMessage(this.code, param);
        this.data = data;
    }
}
