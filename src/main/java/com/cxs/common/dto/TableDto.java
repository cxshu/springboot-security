package com.cxs.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class TableDto {
    private Integer value;
    private String field;
    private String type;
    private String comment;
}
