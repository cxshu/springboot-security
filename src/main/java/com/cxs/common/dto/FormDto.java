package com.cxs.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * 前端表单属性dto
 */
@Data
@AllArgsConstructor
public class FormDto {

    /**
     * 表单项的字段名
     */
    private String field;

    /**
     * 表单项的标签
     */
    private String label;

    /**
     * 表单项的类型
     */
    private String type;

    /**
     * 表单项的宽度
     */
    private String width;

    /**
     * 禁用表单项
     */
    private boolean disabled;

    /**
     *
     */
    private String valuekey;
    private String labelkey;
    private String commentkey;

    /**
     * select/radio/checkbox的选项
     */
    private List<Option> options;

    @Data
    @AllArgsConstructor
    public static class Option {
        private String value;
        private String label;
        private String comment;
    }
}
