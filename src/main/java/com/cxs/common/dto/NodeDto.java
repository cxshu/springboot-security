package com.cxs.common.dto;

import lombok.Data;

import java.util.List;

@Data
public class NodeDto {
    private Long id;
    private String label;
    private List<NodeDto> children = null;
}
