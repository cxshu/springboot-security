package com.cxs.common.lang;

public interface Const {

    String DEFAULT_PASSWORD = "888888";

    /**
     * 状态， 1: 正常
     */
    Integer STATUS_ON = 1;

    /**
     * 状态， 0: 禁用
     */
    Integer STATUS_OFF = 0;

    Integer MENU_TYPE_PARENT = 0;
    Integer MENU_TYPE_MENU = 1;
    Integer MENU_TYPE_BUTTON = 2;
    /**
     * 存储验证码的Key
     */
    String CAPTCHA_KEY = "captcha";

    /**
     * 存储权限的缓存key
     */
    String CACHE_KEY_AUTHORITY = "authority";

    /**
     * 存储菜单的缓存key
     */
    String CACHE_KEY_MENU = "menus";

    String CACHE_KEY_NAV = "nav";

}
