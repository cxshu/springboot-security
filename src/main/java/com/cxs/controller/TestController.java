package com.cxs.controller;

import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import com.cxs.common.dto.MenuDto;
import com.cxs.controller.base.BaseController;
import com.cxs.entity.Permission;
import com.cxs.entity.User;
import com.cxs.service.IPermissionService;
import com.cxs.service.IUserService;
import com.cxs.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.cxs.common.lang.Const.*;

@RestController
@RequestMapping("/test")
public class TestController extends BaseController {

    @Autowired
    IUserService userService;

    @GetMapping("/user/{id}")
    public User getUser(@PathVariable Long id) {
        return userService.getById(id);
    }

    @GetMapping("/username")
    public User getUserByUsername(String username) {
        return userService.getByUsername(username);
    }

    @PostMapping("/user")
    public boolean updateUser(@RequestBody User user) {
        userService.updateUserById(user);
        return true;
    }

}
