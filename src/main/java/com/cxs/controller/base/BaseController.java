package com.cxs.controller.base;

import com.cxs.service.IPermissionService;
import com.cxs.service.IUserService;
import com.cxs.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;

/**
 * 注入一些常用的bean
 */
public class BaseController {
    @Autowired
    protected HttpServletRequest req;

    @Autowired
    protected RedisUtil redisUtil;

    @Autowired
    protected IUserService userService;

    @Autowired
    protected IPermissionService permissionService;

    protected void delAuthCache(String username) {
        redisUtil.hdel("authority", username);
    }
}
